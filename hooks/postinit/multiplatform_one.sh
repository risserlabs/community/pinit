#!/bin/sh

MULTIPLATFORM_ONE_EXAMPLE=$_BLUEPRINT_TEMP/example

GIT_LFS_SKIP_SMUDGE=1 git clone --depth=1 https://gitlab.com/bitspur/multiplatform.one/multiplatform.one.git $MULTIPLATFORM_ONE_EXAMPLE

for i in $(ls $MULTIPLATFORM_ONE_EXAMPLE); do
    if [ ! -d $i ] && [ ! -f $i ]; then
        cp -r $MULTIPLATFORM_ONE_EXAMPLE/$i ./$i
    fi
done

git lfs track 'platforms/storybook/.loki/reference'
git lfs track 'platforms/storybook/.loki/reference/**/*'

rm -rf packages
sed -i "/'..\/..\/..\/packages\/components\/src',/d" platforms/storybook/.storybook/main.ts
sed -i '/"@multiplatform.one\/components": \["\.\/packages\/components\/\*"\],/d' tsconfig.json
sed -i 's/{ "path": "\.\/packages\/components" },\?/ /g' tsconfig.json
sed -i '/..\/..\/..\/packages\/\*\/src\/\*\*\/\*.stories.@(js|jsx|ts|tsx|mdx)/d' platforms/storybook-expo/.storybook/main.js
yarn install




