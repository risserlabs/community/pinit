#!/bin/sh

[ -f .git ] && true || git init

if ! (cat .gitignore 2>/dev/null | grep -q '.mkpm/'); then
_APP_GITIGNORE=$( (cat .gitignore 2>/dev/null || true) | \
    grep -vE '^#' | sed '/^$/d' | sort )
    if [ "$_APP_GITIGNORE" != "" ]; then
        _APP_GITIGNORE="#app
$_APP_GITIGNORE"
    fi
cat <<EOF > .gitignore
$_APP_GITIGNORE

# mkpm
.mkpm/
EOF
fi

sed -i '/./,$!d' .gitignore
