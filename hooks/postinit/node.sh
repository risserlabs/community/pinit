#!/bin/sh

if ! (cat .gitignore 2>/dev/null | grep -q '.yarn/install-state.gz'); then
cat <<EOF >> .gitignore

# nodejs
$(curl https://raw.githubusercontent.com/github/gitignore/main/Node.gitignore | \
    grep -vE '^#' | sed '/^$/d' | sort)
EOF
fi

sh "$SCRIPTS_PATH/yarn2init.sh"

sh "$SCRIPTS_PATH/register-vscode-extension.sh" 'amodio.tsl-problem-matcher'
sh "$SCRIPTS_PATH/register-vscode-extension.sh" 'dbaeumer.vscode-eslint'
sh "$SCRIPTS_PATH/register-vscode-extension.sh" 'esbenp.prettier-vscode'
