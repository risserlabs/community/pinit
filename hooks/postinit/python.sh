#!/bin/sh

if ! (cat .gitignore 2>/dev/null | grep -q '__pycache__/'); then
cat <<EOF >> .gitignore

# python
$(curl https://raw.githubusercontent.com/github/gitignore/main/Python.gitignore | \
    grep -vE '^#' | sed '/^$/d' | sort)
EOF
fi

sh "$SCRIPTS_PATH/register-vscode-extension.sh" 'ms-python.python'
sh "$SCRIPTS_PATH/register-vscode-extension.sh" 'bungcip.better-toml'

git lfs track poetry.lock
