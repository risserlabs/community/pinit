# File: /blueprint.0.mk
# Project: blueprint
# File Created: 20-02-2022 19:44:08
# Author: Clay Risser
# -----
# Last Modified: 17-05-2023 05:14:23
# Modified By: Clay Risser
# -----
# BitSpur (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.DEFAULT_GOAL: blueprint

export SCRIPTS_PATH := $(CURDIR)/scripts
TMPL := sh $(SCRIPTS_PATH)/tmpl.sh
NULL := /dev/null
NOFAIL := 2>$(NULL) || true

.PHONY: blueprint
blueprint:
ifeq ($(BLUEPRINT_TEST),1)
	@echo ROOT $(BLUEPRINT_ROOT)
	@echo TEMP $(CURDIR)
endif
	@. $(CURDIR)/scripts/questions.sh && \
		$(MAKE) -sf blueprint.1.mk
