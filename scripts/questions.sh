#!/bin/sh

read -p 'What is the project name? ' BLUEPRINT_PROJECT_NAME
export BLUEPRINT_PROJECT_NAME

read -p 'What is the project description? ' BLUEPRINT_PROJECT_DESCRIPTION
export BLUEPRINT_PROJECT_DESCRIPTION

read -p 'What is the project version? ' BLUEPRINT_PROJECT_VERSION
export BLUEPRINT_PROJECT_VERSION

read -p 'What is the project author? ' BLUEPRINT_PROJECT_AUTHOR
export BLUEPRINT_PROJECT_AUTHOR

read -p 'What is the project company? ' BLUEPRINT_PROJECT_COMPANY
export BLUEPRINT_PROJECT_COMPANY

read -p 'What is the project company website? ' BLUEPRINT_PROJECT_COMPANY_WEBSITE
export BLUEPRINT_PROJECT_COMPANY_WEBSITE

read -p 'What is the project repository? ' BLUEPRINT_PROJECT_REPOSITORY
export BLUEPRINT_PROJECT_REPOSITORY

while true; do
    read -p 'Is this a node project? [y/N] ' yn
    case $yn in
        [Yy]* ) export BLUEPRINT_IS_NODE=1; break;;
        * ) break;;
    esac
done

while true; do
    read -p 'Is this a python project? [y/N] ' yn
    case $yn in
        [Yy]* ) export BLUEPRINT_IS_PYTHON=1; break;;
        * ) break;;
    esac
done

while true; do
    read -p 'Should I bootstrap a new project from scratch [y/N] ' yn
    case $yn in
        [Yy]* ) export BLUEPRINT_BOOTSTRAP=1; break;;
        * ) break;;
    esac
done

if [ "$BLUEPRINT_IS_NODE" = "1" ] && [ "$BLUEPRINT_BOOTSTRAP" = "1" ]; then
    while true; do
        read -p 'Is this a multiplatform.one project? [y/N] ' yn
        case $yn in
            [Yy]* ) export BLUEPRINT_IS_MULTIPLATFORM_ONE=1; break;;
            * ) break;;
        esac
    done
fi
