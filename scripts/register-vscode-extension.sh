#!/bin/sh

if [ ! -f .vscode/extensions.json ]; then
    mkdir -p .vscode
    cat <<EOF > .vscode/extensions.json
{
  "recommendations": []
}
EOF
fi

(export _JSON=$(node -e \
    "let j=require('${EXTENSIONS_PATH:-./.vscode/extensions.json}');j.recommendations.push('$1');console.log(JSON.stringify(j,null,2))") && \
    printf '%s\n' "$_JSON" > "${EXTENSIONS_PATH:-./.vscode/extensions.json}")
