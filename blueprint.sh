#!/bin/sh

VERSION=0.0.1

export _GITLAB_PROJECT_ID=33897431
export _GITLAB_PROJECT_NAME=blueprint
export _BLUEPRINT_REMOTE=git@gitlab.com:bitspur/community/blueprint.git
export _BLUEPRINT_TAR="https://gitlab.com/api/v4/projects/$_GITLAB_PROJECT_ID/packages/generic/$_GITLAB_PROJECT_NAME/$VERSION/blueprint.tar.gz"
export _BLUEPRINT_TEMP=${XDG_RUNTIME_DIR:-${TMPDIR:-${TMP:-${TEMP:-/tmp}}}}/$(pwd | sed 's|\/|_|g')
export _BLUEPRINT_TEST=$( ((cd .. && git remote -v 2>/dev/null || true) | grep -q "$_BLUEPRINT_REMOTE") && echo 1 || echo 0 )

rm -rf $_BLUEPRINT_TEMP
mkdir -p $_BLUEPRINT_TEMP

if [ "$_BLUEPRINT_TEST" = "1" ] && [ "$TEST" != "0" ]; then
    echo 'WARN: test mode'
    (cd .. && cp -r hooks scripts templates blueprint.*.mk $_BLUEPRINT_TEMP)
    export BLUEPRINT_TEST=1
else
    (cd $_BLUEPRINT_TEMP && \
        $(curl --version >/dev/null 2>/dev/null && \
            echo curl -Lo || \
            echo wget -O) blueprint.tar.gz $_BLUEPRINT_TAR >/dev/null && \
        tar -xzvf blueprint.tar.gz >/dev/null)
fi

if [ "$BLUEPRINT_ROOT" = "" ]; then
    export BLUEPRINT_ROOT=$(pwd)
fi

make -sC $_BLUEPRINT_TEMP -f $_BLUEPRINT_TEMP/blueprint.0.mk
