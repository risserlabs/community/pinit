# File: /blueprint.2.mk
# Project: blueprint
# File Created: 20-02-2022 19:44:08
# Author: Clay Risser
# -----
# Last Modified: 17-05-2023 05:14:23
# Modified By: Clay Risser
# -----
# BitSpur (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.DEFAULT_GOAL: blueprint

POSTINIT_PATH := $(CURDIR)/hooks/postinit
export SCRIPTS_PATH := $(CURDIR)/scripts
TMPL := sh $(SCRIPTS_PATH)/tmpl.sh
NULL := /dev/null
NOFAIL := 2>$(NULL) || true
OUT := $(addprefix $(BLUEPRINT_ROOT)/,$(shell cd $(CURDIR)/out && \
	find . -type f | sed 's|^\.\/||g'))

.PHONY: blueprint
blueprint: | $(OUT) postinit

$(OUT):
	@mkdir -p $(@D)
	@cp $(CURDIR)/out/$(subst $(BLUEPRINT_ROOT)/,,$@) $@
	@echo created $@

.PHONY: postinit
postinit:
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/shared_begin.sh
ifeq ($(BLUEPRINT_IS_NODE),1)
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/node.sh
endif
ifeq ($(BLUEPRINT_IS_PYTHON),1)
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/python.sh
endif
ifeq ($(BLUEPRINT_BOOTSTRAP),1)
ifeq ($(BLUEPRINT_IS_NODE),1)
ifeq ($(BLUEPRINT_IS_MULTIPLATFORM_ONE),1)
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/multiplatform_one.sh
else
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/node_bootstrap.sh
endif
endif
ifeq ($(BLUEPRINT_IS_PYTHON),1)
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/python_bootstrap.sh
endif
endif
	@cd $(BLUEPRINT_ROOT) && sh $(POSTINIT_PATH)/shared_end.sh
