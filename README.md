# blueprint

> initialize projects using BitSpur coding standards

## Usage

### Without Install

You can use blueprint without installing it. Simply run the following
command in the project you want to initialize.

```sh
sh <($(curl --version >/dev/null 2>/dev/null && echo curl -L || echo wget -O-) https://gitlab.com/api/v4/projects/33897431/packages/generic/blueprint/0.0.1/blueprint.sh)
```

### With Install

Run the following command or use the Makefile `make install` to install.

```sh
sudo $(curl --version >/dev/null 2>/dev/null && echo curl -Lo || echo wget -O) /usr/local/bin/blueprint https://gitlab.com/api/v4/projects/33897431/packages/generic/blueprint/0.0.1/blueprint.sh && sudo chmod +x /usr/local/bin/blueprint
```

You can use it by running the following command in the project you want to initialize.

```sh
blueprint
```

## Development

| command          | description                 |
| ---------------- | --------------------------- |
| `mkpm clean`     | clean project               |
| `mkpm example`   | test blueprint with example |
| `mkpm install`   | install blueprint           |
| `mkpm package`   | package blueprint           |
| `mkpm purge`     | pure project                |
| `mkpm reinstall` | reinstall blueprint         |
| `mkpm uninstall` | uninstall blueprint         |
