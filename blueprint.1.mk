# File: /blueprint.1.mk
# Project: blueprint
# File Created: 20-02-2022 19:44:08
# Author: Clay Risser
# -----
# Last Modified: 17-05-2023 05:14:23
# Modified By: Clay Risser
# -----
# BitSpur (c) Copyright 2022
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.DEFAULT_GOAL: blueprint

PREINIT_PATH := $(CURDIR)/hooks/preinit
export SCRIPTS_PATH := $(CURDIR)/scripts
TMPL := sh $(SCRIPTS_PATH)/tmpl.sh
NULL := /dev/null
NOFAIL := 2>$(NULL) || true

.PHONY: blueprint
blueprint:
	@mkdir -p $(CURDIR)/out
	@$(call tmpl,shared)
ifeq ($(BLUEPRINT_IS_NODE),1)
	@$(call tmpl,node)
endif
ifeq ($(BLUEPRINT_IS_PYTHON),1)
	@$(call tmpl,python)
endif
ifeq ($(BLUEPRINT_BOOTSTRAP),1)
ifeq ($(BLUEPRINT_IS_NODE),1)
ifeq ($(BLUEPRINT_IS_MULTIPLATFORM_ONE),1)
	@$(call tmpl,multiplatform_one)
else
	@$(call tmpl,node_bootstrap)
endif
endif
ifeq ($(BLUEPRINT_IS_PYTHON),1)
	@$(call tmpl,python_bootstrap)
endif
endif
	@$(MAKE) -sf $(CURDIR)/blueprint.1.mk preinit
	@$(MAKE) -sf $(CURDIR)/blueprint.2.mk

.PHONY: preinit
preinit:
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/shared_begin.sh
ifeq ($(BLUEPRINT_IS_NODE),1)
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/node.sh
endif
ifeq ($(BLUEPRINT_IS_PYTHON),1)
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/python.sh
endif
ifeq ($(BLUEPRINT_BOOTSTRAP),1)
ifeq ($(BLUEPRINT_IS_NODE),1)
ifeq ($(BLUEPRINT_IS_MULTIPLATFORM_ONE),1)
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/multiplatform_one.sh
else
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/node_bootstrap.sh
endif
endif
ifeq ($(BLUEPRINT_IS_PYTHON),1)
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/python_bootstrap.sh
endif
endif
	@cd $(BLUEPRINT_ROOT) && sh $(PREINIT_PATH)/shared_end.sh

define tmpl
mkdir -p $(CURDIR)/templates/$1 && \
cd $(CURDIR)/templates/$1 && \
for f in $$(find . -type f | $(SED) 's|^\.\/||g'); do \
	export _MKDIR=$$($(ECHO) $$f | $(SED) 's|/\?[^/]\+$$||g' $(NOFAIL)) && \
	if [ "$$_MKDIR" != "" ]; then \
		$(MKDIR) -p $(CURDIR)/out/$$_MKDIR ; \
	fi && \
	if [ "$$(echo $$f | grep -oE '\.tmpl$$' $(NOFAIL))" = ".tmpl" ]; then \
		$(TMPL) $(CURDIR)/templates/$1/$$f > $(CURDIR)/out/$$(echo $$f | $(SED) 's|\.tmpl$$||g'); \
	else \
		$(CP) $(CURDIR)/templates/$1/$$f $(CURDIR)/out/$$f; \
	fi; \
done
endef
